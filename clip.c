#include "bgi.h"

rect_t vp = { 0, 0, 0, 0 };

void Clip(coord_t x1, coord_t y1, coord_t x2, coord_t y2)
{
  vp.x1 = x1;
  vp.y1 = y1;
  vp.x2 = x2;
  vp.y2 = y2;
}
