.HOLD
.EXTENSIONS:

.EXTENSIONS: .obj .asm .c

CPU = -0

.asm.obj:
	jwasm $(CPU) -I. -Gd -Fo$@ $<

CCOPTS = -ms $(CPU) -I. -ecc -s -ox -oe -oh -ok -ol+ -os &
         -zdp -zp1 -zff -zgf -zu -zl -zls -wx

.c.obj: .AUTODEPEND
	*wcc $(CCOPTS) -fo$@ $<

vga256.bgi: c0bgi.obj bgi.lib
	*wlink format dos output raw name $@ file c0bgi.obj,bgi.lib option nodefaultlibs,map,eliminate,farcalls,caseexact,showdead,verbose

bgi.lib: install.obj clip.obj move.obj color.obj bitmap.obj line.obj &
         patbar.obj flood.obj text.obj pixelc.obj pixel.obj palette.obj
	*wlib -b -c -fo$@ $?

distclean: clean .SYMBOLIC
!ifdef __UNIX__
	rm -rf *.{bgi,lib}
!endif
!ifdef __MSDOS__
	del *.bgi
	del *.lib
!endif

clean: .SYMBOLIC
!ifdef __UNIX__
	rm -rf *.{obj,lst,bak,map,err}
!endif
!ifdef __MSDOS__
	del *.obj
	del *.lst
	del *.bak
	del *.map
	del *.err
!endif
