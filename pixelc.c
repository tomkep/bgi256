#include "bgi.h"

PutPixelFuncPtr_t PutPixelFunc = PutPixel;

PutPixelFuncPtr_t GetPutPixelFunc(uint16_t mode)
{
  static PutPixelFuncPtr_t const T[] =
  {
    PutPixel,
    XorPutPixel,
    OrPutPixel,
    AndPutPixel,
    NotPutPixel
  };
  return(mode < sizeof(T) / sizeof(T[0]) ? T[mode] : PutPixel);
}

void setWriteMode(uint16_t mode)
{
  PutPixelFunc = GetPutPixelFunc(mode);
}
