PSTRING         MACRO   s
                LOCAL   b, e
                db      OFFSET e - OFFSET b
        b       db      s
        e       db      0
ENDM

BGIHEADER       MACRO   name, drvEnd
                LOCAL   HDR20, hdrEnd@
HDR20           STRUC
                dw      ?       ; header size
                dw      0       ; driver number
                dw      ?       ; driver size
                db      2       ; driver version
                db      0       ; driver revision
                db      1       ; minimum driver version
                db      1       ; minimum driver revision
HDR20           ENDS
                db      'pk', 8, 8
                db      'BGI Device Driver (', name, ') 2.00', 13, 10
                db      'Copyright (c) 2011 Tomasz Kepczynski', 13, 10
                db      0, 26
                HDR20   <hdrEnd@, 0, drvEnd, 2, 0, 1, 1>
                ORG     80h
                HDR20   <hdrEnd@, 0, drvEnd, 2, 0, 1, 1>
                PSTRING name
                ALIGN   16
hdrEnd@         LABEL   BYTE
ENDM

HEADER          SEGMENT     PARA    PUBLIC  USE16 'HEADER'
BGIHEADER       'VGA256', DrvEnd@
HEADER          ENDS

_TEXT           SEGMENT     WORD    PUBLIC  USE16 'CODE'

BGIDRV          PROC    FAR PRIVATE
                push    ds              ; (push ds, push cs are signature)
                push    cs              ; make ds=cs
                pop     ds
                ASSUME  ds:DGROUP
                cld
                push    bp
                call    [DDOVEC@+si]    ; call the local function
                pop     bp
                pop     ds
                ASSUME  ds:NOTHING
                ret
BGIDRV          ENDP

                db      43h, 42h

                ORG     BGIDRV + 10h
emulate@        PROC    NEAR PRIVATE    ; emulate function (nop until
                ret                     ; patched by loader)
emulate@        ENDP

                ORG     BGIDRV + 15h
reserved@       PROC    NEAR PRIVATE    ; near ret for unused functions
                ret
reserved@       ENDP

EXTERNDEF       InstallDriver   : PROC
EXTERNDEF       ModeQuery       : PROC
EXTERNDEF       ModeName        : PROC
EXTERNDEF       Init            : PROC
EXTERNDEF       Clear           : PROC
EXTERNDEF       Post            : PROC
EXTERNDEF       Move            : PROC
EXTERNDEF       Draw            : PROC
EXTERNDEF       Vect            : PROC
EXTERNDEF       PatBar          : PROC
;PALETTE
;ALLPALETTE
EXTERNDEF       Color           : PROC
EXTERNDEF       setFillStyle    : PROC
EXTERNDEF       setLineStyle    : PROC
EXTERNDEF       setTextStyle    : PROC
EXTERNDEF       Text            : PROC
EXTERNDEF       getTextSize     : PROC
EXTERNDEF       FloodFill       : PROC
EXTERNDEF       GetPixel        : PROC
EXTERNDEF       PutPixel        : PROC
;BitmapUtil                                             ; !!!
EXTERNDEF       setDrawPage     : PROC
EXTERNDEF       setVisualPage   : PROC
EXTERNDEF       setWriteMode    : PROC
EXTERNDEF       SaveBitmap      : PROC
EXTERNDEF       RestoreBitmap   : PROC
EXTERNDEF       Clip            : PROC
;COLOR_QUERY
EXTERNDEF       getColorTableSize       : PROC
EXTERNDEF       getDefaultColorTable    : PROC

                ASSUME  ds:DGROUP

install@@       PROC
                cmp     al,2
                ja      @@1
IF @Cpu AND 8
                movzx   eax,al
                jmp     [@@jumpTbl1+2*eax]
ELSE
                mov     bl,al
                xor     bh,bh
                shl     bx,1
                jmp     [@@jumpTbl1+bx]
ENDIF
    @@jumpTbl1  dw      InstallDriver
                dw      ModeQuery
                dw      ModeName
    @@1:        ret
install@@       ENDP

palette@@       PROC
                ; remove it later to its own file
                test    ah,80h
                jz      @@1
                test    ah,40h
                jz      @@2
                xor     al,al
    @@1:        mov     bh,bl       ; color value
                mov     bl,al       ; pallette reg. number
                mov     ax,1000h
                int     10h
                ret
    @@2:        mov     dh,bl
                mov     ch,cl
                mov     cl,dl
                xor     bh,bh
                mov     bl,al
                mov     ax,1010h
                int     10h
                ret
palette@@       ENDP

allpalette@@    PROC
                mov     dx,bx
                mov     ax,1002h
                int     10h
                ret
allpalette@@    ENDP

bitmaputil@@    PROC
                mov     bx,cs
                mov     es,bx
                mov     bx,offset @@BitmapUtilTbl
                ret
@@BitmapUtilTbl dw      _fgotographic@@
                dw      _fexitgraphic@@
                dw      _fputpixel@@
                dw      _fgetpixel@@
                dw      _fgetpixbyte@@
                dw      _fsetdrawpage@@
                dw      _fsetvispage@@
                dw      _fsetwrtmode@@
bitmaputil@@    ENDP

_fgotographic@@ PROC    FAR
                ret
_fgotographic@@ ENDP

_fexitgraphic@@ PROC    FAR
                ret
_fexitgraphic@@ ENDP

_fputpixel@@    PROC    FAR USES ds
                mov     cx,cs
                mov     ds,cx
                call    PutPixel
                ret
_fputpixel@@    ENDP

_fgetpixel@@    PROC    FAR USES ds
                mov     cx,cs
                mov     ds,cx
                call    GetPixel
                ret
_fgetpixel@@    ENDP

_fgetpixbyte@@  PROC    FAR
                mov     ax,8
                ret
_fgetpixbyte@@  ENDP

_fsetdrawpage@@ PROC    FAR USES ds
                mov     cx,cs
                mov     ds,cx
                call    setDrawPage
                ret
_fsetdrawpage@@ ENDP

_fsetvispage@@  PROC    FAR USES ds
                mov     cx,cs
                mov     ds,cx
                call    setVisualPage
                ret
_fsetvispage@@  ENDP

_fsetwrtmode@@  PROC    FAR USES ds
                mov     cx,cs
                mov     ds,cx
                call    setWriteMode
                ret
_fsetwrtmode@@  ENDP

querycolor@@    PROC
                cmp     al,1
                ja      @@1
IF @Cpu AND 8
                movzx   eax,al
                jmp     [@@jumpTbl2+2*eax]
ELSE
                mov     bl,al
                xor     bh,bh
                shl     bx,1
                jmp     [@@jumpTbl2+bx]
ENDIF
    @@jumpTbl2  dw      getColorTableSize
                dw      getDefaultColorTable
    @@1:        ret
querycolor@@    ENDP

_TEXT           ENDS

_DATA           SEGMENT     WORD    PUBLIC  USE16 'DATA'
DDOVEC@         dw      install@@
                dw      Init
                dw      Clear
                dw      Post
                dw      Move
                dw      Draw
                dw      Vect
                dw      emulate@
                dw      emulate@
                dw      PatBar
                dw      emulate@
                dw      emulate@
                dw      emulate@
                dw      palette@@
                dw      allpalette@@
                dw      Color
                dw      setFillStyle
                dw      setLineStyle
                dw      setTextStyle
                dw      Text
                dw      getTextSize
                dw      reserved@
                dw      FloodFill
                dw      GetPixel
                dw      PutPixel
                dw      bitmaputil@@
                dw      SaveBitmap
                dw      RestoreBitmap
                dw      Clip
                dw      querycolor@@
                dw      35 DUP (reserved@)
_DATA           ENDS

CONST           SEGMENT     WORD    PUBLIC  USE16 'DATA'
CONST           ENDS

CONST2          SEGMENT     WORD    PUBLIC  USE16 'DATA'
CONST2          ENDS

_BSS            SEGMENT     WORD    PUBLIC  USE16 'BSS'
_BSS            ENDS

_END            SEGMENT     BYTE    PRIVATE USE16
DrvEnd@         LABEL       BYTE
_END            ENDS

DGROUP  GROUP _TEXT, _DATA, CONST, CONST2, _BSS, _END

END
