#include "bgi.h"

typedef struct
{
  color_t fore;
  color_t back;
  color_t fill;
} Colors_t;

static Colors_t colors = { 0, 0, 0 };

void Color(color_param_t c)
{
  colors.fore = c.fore;
  colors.fill = c.fill;
}

color_t getDrawColor(bool_t draw)
{
  return(draw ? colors.fore : colors.back);
}

color_t getFillColor(bool_t draw)
{
  return(draw ? colors.fill : colors.back);
}

color_t getBkgrColor(void)
{
  return(colors.back);
}

void setBkgrColor(color_t bkgr)
{
  colors.back = bkgr;
}
