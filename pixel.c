#include "bgi.h"

#define PIXEL_ADDRESS(x, y) (0xA000:>(color_t _near *)((y) * 320 + (x)))

color_t GetPixel(coord_t x, coord_t y)
{
  return(*PIXEL_ADDRESS(x, y));
}

void PutPixel(coord_t x, coord_t y, color_t color)
{
  *PIXEL_ADDRESS(x, y) = color;
}

void XorPutPixel(coord_t x, coord_t y, color_t color)
{
  *PIXEL_ADDRESS(x, y) ^= color;
}

void OrPutPixel(coord_t x, coord_t y, color_t color)
{
  *PIXEL_ADDRESS(x, y) |= color;
}

void AndPutPixel(coord_t x, coord_t y, color_t color)
{
  *PIXEL_ADDRESS(x, y) &= color;
}

void NotPutPixel(coord_t x, coord_t y, color_t color)
{
  *PIXEL_ADDRESS(x, y) = ~color;
}
