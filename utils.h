#ifndef __UTILS_H
#define __UTILS_H

#include "types.h"

#ifdef __cplusplus
extern "C"
{
#endif

int16_t  abs(int16_t);
uint16_t _rotl(uint16_t value, int16_t count);
uint16_t _rotr(uint16_t value, int16_t count);
#if defined(__WATCOMC__)
#pragma intrinsic(abs, _rotl, _rotr)
#endif          // defined(__WATCOMC__)

#ifdef __cplusplus
}
#endif

#endif /* __UTILS_H */
