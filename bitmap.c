#include "types.h"
#include "bgi.h"

struct Bitmap_t
{
  uint16_t dx;
  uint16_t dy;
  uint8_t  bitmap[1];
};

void SaveBitmap(coord_t x1, coord_t y1, struct Bitmap_t _far *p)
{
  uint8_t _far *pp = p->bitmap;
  coord_t x, x2, y, y2;
  for(y = y1, y2 = y1 + p->dy; y <= y2; ++y)
    for(x = x1, x2 = x1 + p->dx; x <= x2; ++x)
      *pp++ = GetPixel(x, y);
}

void RestoreBitmap(coord_t x1, coord_t y1, uint8_t mode, struct Bitmap_t _far *p)
{
  uint8_t _far *pp = p->bitmap;
  PutPixelFuncPtr_t const PutPixelFunc = GetPutPixelFunc(mode);
  coord_t x, x2, y, y2;
  for(y = y1, y2 = y1 + p->dy; y <= y2; ++y)
    for(x = x1, x2 = x1 + p->dx; x <= x2; ++x)
      PutPixelFunc(x, y, *pp++);
}
