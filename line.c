#include "types.h"
#include "bgi.h"
#include "utils.h"

static uint16_t const PredefinedLineStyle[4] =
{
  0xFFFF, /* SolidLineStyle  */
  0xCCCC, /* DottedLineStyle */
  0xFC78, /* CenterLineStyle */
  0xF8F8  /* DashedLineStyle */
};

struct LineStyle_t
{
  uint16_t style;
  uint16_t width;
};

static struct LineStyle_t LineStyle = { 0xFFFF, 1 };

void setLineStyle(uint8_t Pat, uint16_t UserPat, uint16_t LineWidth)
{
  LineStyle.style = Pat == 4 ? UserPat : PredefinedLineStyle[Pat];
  LineStyle.width = LineWidth;
}

void Vect(coord_t x1, coord_t y1, coord_t x2, coord_t y2)
{
  int16_t deltaX = abs(x1 - x2), deltaY = abs(y1 - y2);
  int16_t stepX = x1 > x2 ? -1 : 1, stepY = y1 > y2 ? -1 : 1;
  int16_t c = deltaX ? 0 : -1;
  uint16_t mask = LineStyle.style;
  while(1)
  {
    mask = _rotl(mask, 1);
    PutPixelFunc(x1, y1, getDrawColor(mask & 1));
    if(x1 == x2 && y1 == y2)
      break;
    if(c < 0)
    {
      y1 += stepY;
      c += deltaX;
    }
    else
    {
      x1 += stepX;
      c -= deltaY;
    }
  }
}

void Draw(coord_t x, coord_t y)
{
  Vect(cp.x, cp.y, x, y);
  Move(x, y);
}
