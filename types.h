#ifndef __TYPES_H
#define __TYPES_H

typedef signed char   int8_t;
typedef int           int16_t;
typedef long          int32_t;

typedef unsigned char uint8_t;
typedef unsigned      uint16_t;
typedef unsigned long uint32_t;

typedef unsigned      bool_t;
#define false         0
#define true          (!false)

typedef unsigned      size_t;

#endif /* __TYPES_H */
