#include "types.h"
#include "bgi.h"

BGIStatus_t status =
{
  0,     /* status   */
  0,     /* type     */
  319,   /* xres     */
  199,   /* yres     */
  319,   /* xefres   */
  199,   /* yefres   */
  10000, /* xinch    */
  10000, /* yinch    */
  10000, /* aspect   */
  8,     /* chsizex  */
  8,     /* chsizey  */
  255,   /* fcolors  */
  255    /* bcolors  */
};

CharImage_t _far *getFont8x8(void);
#pragma aux getFont8x8 = \
  "push  bp" \
  "mov   ax,0x1130" \
  "mov   bh,3" \
  "int   0x10" \
  "mov   bx,bp" \
  "pop   bp" \
  value [ES BX] \
  modify [AX]

BGIStatus_t _far *InstallDriver(uint8_t ModeNo)
{
  Move(0, 0);
  Clip(0, 0, status.xres, status.yres);
  font8x8 = getFont8x8();
  status.status = ModeNo == 0 ? grOk : grError;
  return(&status);
}

uint16_t ModeQuery()
{
  return(1);
}

void const _far *ModeName(uint16_t ModeNo)
{
  return(ModeNo == 0 ? "\013320x200x256" : 0);
}

struct DIT
{
  uint8_t bkgrColor;
  uint8_t initFlag;
  uint8_t reserved[64];
};

void setVga256Mode(void);
#pragma aux setVga256Mode = \
  "mov   ax,0x13" \
  "int   0x10"

void setRGBPalette(RGBPaletteEntry_t _far *palette);
#pragma aux setRGBPalette = \
  "xor   bx,bx" \
  "mov   cx,256" \
  "mov   ax,0x1012" \
  "int   0x10" \
  parm [ES DX]

void Init(struct DIT _far *p)
{
  if(p->initFlag != 0xA5)
  {
    setBkgrColor(p->bkgrColor);
    setVga256Mode();
    setRGBPalette(DefaultRGBPalette);
    Clear();
  }
}

void Post(void)
{
}

void setDrawPage(uint8_t page)
{
  status.status = grError;
}

void setVisualPage(uint8_t page)
{
  status.status = grError;
}

uint32_t getColorTableSize()
{
  return(((uint32_t)status.fcolors << 16)  | ((uint16_t)status.fcolors + 1));
}
#pragma aux getColorTableSize value [CX BX]

void const _far *getDefaultColorTable()
{
  static uint8_t const __based(__segname("_CODE")) DefColorTable[] =
  {
    16, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0
  };
  return(DefColorTable);
}
#pragma aux getDefaultColorTable value [ES BX]
